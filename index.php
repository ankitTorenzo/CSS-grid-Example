<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/indexStyle.css">
    <title>Index</title>
</head>
<body>
<ul>
<?php
$a = scandir('./');
foreach ($a as $file) {
	if (strstr($file, '.php') || strstr($file, '.html')) {

	    if ($file == 'index.php' || $file == 'AllFunctions.php')continue;
		echo "<li><a href='$file' target='_blank'>$file</a></li>";
	}
}
?>
</ul>
</body>
</html>