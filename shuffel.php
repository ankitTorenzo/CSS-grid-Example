<?php
require_once ("AllFunctions.php");
/**
 * Created by PhpStorm.
 * User: Abhicenation
 * Date: 11/17/2017
 * Time: 8:28 PM
 */
$arr = array (
	'a'=>2455,
	'b'=>564,
	'c'=>6534,
	'd'=>524,
	'e'=>3524,
	'f'=>24654,
	'g'=>32
);
$selected = array ();
$shuffleArray = array ();
$allDone = false;
$i=0;
while(!$allDone){
	$random = rand(0,count($arr));
	if (!inArray($random,$selected)){
		$selected[count($selected)] = $random;
		$c = 0;
		$i = 0;
		foreach ($arr as $k => $value) {
			if ($c == $random){
				$shuffleArray[$k] = $value;
				$selected[$i] = $random;
			}
			$c++;
		}
		if (count($shuffleArray) === count($arr)){
			$allDone = true;
		}
	}
}
echo '<pre>';
foreach ($shuffleArray as $item => $value) {
		echo "$item => $value <br>";
	}
echo '</pre>';