<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sorting 2DArray By Specific Key</title>
</head>
<body>
<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="get">
	<select name="Key" id="key">
		<option value="name">Name</option>
		<option value="email">Email</option>
		<option value="phone">Phone</option>
		<option value="country">Country</option>
	</select>
	<input type="submit" value="Submit">
</form>
</body>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: Abhicenation
 * Date: 11/17/2017
 * Time: 5:35 PM
 */
$personMaster = array (
	array (
		'name'    => 'Ankit',
		'email'   => 'em1@gmail.com',
		'phone'   => 111315825,
		'country' => 'IND'
	),
	array (
		'name'    => 'sjdhgfgd',
		'email'   => 'emasd@gmail.com',
		'phone'   => 11656345,
		'country' => 'NAKS'
	),
	array (
		'name'    => 'hgkldfg',
		'email'   => 'skljhdfgd@gmail.com',
		'phone'   => 11651464563,
		'country' => 'JAP'
	),
	array (
		'name'    => 'dnvsdshdf',
		'email'   => 'sdjjasdhfjhgf@gmail.com',
		'phone'   => 3213453465,
		'country' => 'USA'
	),
	array (
		'name'    => 'hgskljhdgsafd',
		'email'   => 'asedlhfasjdgsjhd@gmail.com',
		'phone'   => 3246545866,
		'country' => 'AUS'
	)
);
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	$keyExists = false;
	$userKey = $_GET['Key'];
	foreach ($personMaster as $item) {
		foreach ($item as $k => $v) {
			/*echo "$userKey And $k";*/
			if ($userKey == $k) {

				$keyExists = true;
				break 2;
			}
		}
	}
	if ($keyExists) {
		sortArrayByKey($personMaster, $userKey);
	}else {
		echo "Key Does Not Matched";
	}
}
function sortArrayByKey($array, $sortKey)
{
	for ($i = 0; $i < count($array); $i++) {

		for ($j = $i; $j < count($array); $j++) {
			if ($array[$i][$sortKey] > $array[$j][$sortKey]) {
				$temp = $array[$i];
				$array[$i] = $array[$j];
				$array[$j] = $temp;
			}
		}
	}
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}