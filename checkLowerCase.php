<?php

$isUpper = false;
$length = strlen($str);
$upperArray = array();
$j = 0;
for ($i = 0; $i < $length; $i++) {
    if ($str[$i] >= 'A' && $str[$i] <= 'Z') {
        $isUpper = true;
        /*echo "123<br>";*/
    } elseif ($str[$i] >= 'a' && $str[$i] <= 'z') {
        $isUpper = false;
        /*echo "234567<br>";*/
    }

    if ($isUpper && !in_array($str[$i], $upperArray)) {
        $upperArray[$j] = $str[$i];
        $j++;
        /*echo '10<br>';*/
    }
}
if (count($upperArray) > 0) {
    /*echo count($upperArray);*/
    echo "This String is not valid make sure the You change the following chars in Lower Case:- " . implode(',', $upperArray);
} else {
    echo "This String is purely in LowerCase you are good to Go...!";
}